class Article < ApplicationRecord
	has_many :comments, dependent: :destroy
	has_many :ratings, dependent: :destroy
	validates :title, presence: true,
                    length: { minimum: 5 }
    validates :text, presence: true   

   def self.to_csv
    CSV.generate do |csv|
      csv << %w{ title lowest_rating highest_rating average_rating number_of_comments average_length_comments } 
      all.each do |article|

      	if article.ratings.count() != 0
	      	lowest_rating = 5
	      	highest_rating = 1
	      	average_rating = 0.0
	      	article.ratings.each do |rating|
	      		average_rating += rating.score.to_f
	      		if rating.score < lowest_rating
	      			lowest_rating = rating.score
	      		end
	      		if rating.score > highest_rating
	      			highest_rating = rating.score
	      		end
	      	end
	      	average_rating = average_rating/article.ratings.count()
	    end

	    if article.comments.count() != 0
	      	average_length_comments = 0.0
	      	article.comments.each do |comment|
	      		average_length_comments += comment.body.length().to_f
	      	end
	      	average_length_comments = average_length_comments/article.comments.count()
	    end


        csv << [article.title, lowest_rating, highest_rating, average_rating, article.comments.count(), average_length_comments ]
      end
    end
  end   
end
