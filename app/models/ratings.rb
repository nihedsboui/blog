class Rating < ApplicationRecord
  belongs_to :article
  validates_inclusion_of :score, :in => 1..5
end
